<?php

class Product {
    public static $price = 780;
    public static $title = 'Товарчик';

    public static function getPrice(){
        return "Цена товара:" .  self::$price;
    }

    public static function getDescription(){
        //и тут меня осенило, когда нужно использовать static вместо self...
        echo "<h1>Товар: " . static::$title . "</h1><p>" . static::getPrice() ."</p>";
    }
}

class ProductSale extends Product{
    public static $price = 490;

    public static function getPrice(){
        return "Цена товара co скидкой:" .  self::$price;
    }
}

Product::getDescription();
ProductSale::getDescription();

//--------------------------------------------------

// П.5
// т.к. переменная $x статичная, то каждый раз она будет увеличиваться на 1. Выведет 1234
//
// П.6
// произойдет тоже самое, только в этом случае у нас 2 класса,
// и увеличиваться будут переменная каждого класса. Т.е. сначала А::$x увеличится на 1, потом B::$x, потом снова А::$x и B::$x.
// на выходе будет 1122
//
// П.7
// выведет тоже самое 1122. Разница только в том, что при создании объектов скобки опущены.
// Но в классе конструктора нет, поэтому видимо нет и разницы
